/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { assetsConfig } from 'assets/Config/assetsConfig';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router } from '@angular/router';
import { introAnimation } from '../../../assets/Config/keyframes';
/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-introscreens',
    templateUrl: './introscreens.template.html',
    animations: [introAnimation]
})

export class introscreensComponent implements OnInit {

    introList = [];
    previousIndex = 0;
    selectedIndex = 0;
    swipeDirection: string;
    constructor(private router: Router) {
        this.introList = assetsConfig.introList;
    }

    ngOnInit() {
        this.changeSlide(0);
    }

/**
 *  To change the introduction screens
 * @param index : index of the slide in reference with introList array.
 */
    public changeSlide(index) {
        console.log(index)
        if (index >= 0 && this.introList.length) {
            if (index < this.introList.length) {
                this.previousIndex = this.selectedIndex;
                this.selectedIndex = index;
                this.introList[this.previousIndex]['show'] = false;
                this.introList[this.selectedIndex]['show'] = true;
            } else {
               // this.router.navigate(['/home/login']);
            }
        }
    }

/**
 * To detect Swipe Directions
 * @param event = swipe event (right or left)
 * @param index = index of the slide.
 */
    public swipeDetect(event, index) {
        this.swipeDirection = event.type;
        this.changeSlide(index);
    }

    /* when the user clicks on next button goes to the next slide*/

    public nextSlide() {
        this.selectedIndex = (this.selectedIndex > this.introList.length-1) ? 0:this.selectedIndex
        this.previousIndex = this.selectedIndex;
        this.selectedIndex = this.selectedIndex + 1;
        this.introList[this.previousIndex]['show'] = false;
        this.introList[this.selectedIndex]['show'] = true;
    }

    /*
    skips the slides and goes to the last slide  
    */

    public skip(){
        this.changeSlide(3)
    }
}
