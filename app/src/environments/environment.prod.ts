export const environment = {
    "name": "prod",
    "properties": {
        "production": true,
        "ssdURL": "http://localhost:8081/api/",
        "tenantName": "neutrinos-delivery",
        "appName": "image-courosal",
        "namespace": "com.neutrinos-delivery.image-courosal",
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "useDefaultExceptionUI": true,
        "isIDSEnabled": "false",
        "webAppMountpoint": "web"
    }
}