import { keyframes, style, trigger, transition, animate, query, stagger, state } from '@angular/animations';
export const slideOutLeft = [
    style({ transform: 'translate3d(0, 0, 0)', offset: 0 }),
    style({ transform: 'translate3d(-150%, 0, 0)', opacity: 0, offset: 1 }),
];

export const slideOutRight = [
    style({ transform: 'translate3d(0, 0, 0)', offset: 0 }),
    style({ transform: 'translate3d(150%, 0, 0)', opacity: 0, offset: 1 }),
];
export const introAnimation = trigger('slideState', [
    state('swipeleft', style({
    })),
    state('swiperight', style({
    })),
    transition('* => swipeleft', [
        style({ transform: 'translateX(5%)' }),
        animate('0.2s')
    ]),
    transition('* => swiperight', [
        style({ transform: 'translateX(-5%)' }),
        animate('0.2s')
    ])
]);