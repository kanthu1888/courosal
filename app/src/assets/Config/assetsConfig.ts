export const assetsConfig = {
    introList: [{
    title: 'Get your Money under Control',
    description: 'Track your Portfolio on the go',
    show: false
},
{
    title: 'Get your Money under Control',
    description: 'Buy, redeem and switch Mutual Funds digitally',
    show: false
},
{
    title: 'Get your Money under Control',
    description: 'Compare and buy Term and Savings Insurance Plans',
    show: false
},
{
    title: 'Get your Money under Control',
    description: 'Get 360 degree view of your Financial Health',
    show: false
}]
}